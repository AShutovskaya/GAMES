#pragma once
#include<SFML/Graphics.hpp>
#include<game.hpp.hpp>
namespace as
{
	Circle::Circle(int x, int y, float r, float velocity)
	{
		m_x = x;
		m_y = y;
		m_r = r;
		m_velocity = velocity;
		m_circle = new sf::CircleShape(m_r);
		m_circle->setOrigin(m_r, m_r);
		m_circle->setFillColor(sf::Color::Cyan);
		m_circle->setPosition(m_x, m_y);
	}
	Circle::~Circle()
	{
		delete m_circle;
	}

	sf::CircleShape* Circle::Get() { return m_circle; }
	void Circle::Move()
	{
		m_x -= m_velocity;
		m_circle->setPosition(m_x, m_y);
	}
	void Circle::SetX(int x)
	{
		m_x = x;
		m_circle->setPosition(m_x, m_y);

	}
	int Circle::GetX() { return m_x; }
	void Circle::SetVelocity(int velocity)
	{
		m_velocity = velocity;
	}
}

namespace as
{
	Pers::Pers(sf::String F, float X, float Y,float W,float H)
	{
		HeroDirection = 0, speed = 0;
		File = F;
		m_x = 0;
		m_y = 0;
		w = W;
		h = H;
		sf::Event event;
		heroimage.loadFromFile("img/" + File);
		herotexture.loadFromImage(heroimage);//���������� ���� ����������� � ��������
		herosprite.setTexture(herotexture);//�������� ������ ���������
		x3 = X; y3 = Y;//���������� ��������� �������
		herosprite.setTextureRect(sf::IntRect(0, 0, w, h)); //������ ������� ���� ������������� ��� ������ ������ ���������
	}
	Pers::~Pers()
	{
	}
	void Pers::imagine(float time)
	{
		switch (HeroDirection)//��������� ��������� � ����������� �� �����������
		{
		case 0: m_x = -speed; m_y = 0; break;// �������� ���� ������ ������
		case 1: m_x = speed; m_y = 0; break;// �������� ���� ������ �����
		case 2: m_x = 0; m_y = -speed; break;// �������� ���� ������ ����
		case 3: m_x = 0; m_y = speed; break;// �������� ���� ������ �����
		}
		x3 += m_x * time;//�������� �������� ��������� � ��� ��������� ��������
		y3 += m_y * time;
		speed = 0;//�������� ��������, ����� �������� �����������.
		herosprite.setPosition(x3, y3);
	}
}