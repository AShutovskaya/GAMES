#pragma once
#include <SFML/Graphics.hpp>
namespace as
{
	class Circle
	{
	public:
		Circle(int x, int y, float r, float velocity);

		~Circle();
		sf::CircleShape* Get();
		void Move();
		void SetX(int x);
		int GetX();
		void SetVelocity(int velocity);
	private:
		int m_x, m_y;
		float m_r;
		float m_velocity;
		sf::CircleShape* m_circle;
	};
}
namespace as
{
	class Pers
	{
	private:
		float  w, h, m_x , m_y ;
	public:
		float x3, y3, speed; //���������� ������ � � �,��������� (�� � � �� �), ���� ��������
		int HeroDirection = 0;
		sf::String File;
		sf::Texture herotexture;
		sf::Image heroimage;
		sf::Sprite herosprite;
		sf::Clock clock;
		Pers(sf::String F, float X, float Y,float W,float H);
		~Pers();
		void imagine(float time);
	};
}
