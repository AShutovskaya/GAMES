﻿#include<SFML/Graphics.hpp>
#include<iostream>
#include<vector>
#include<thread>
#include<chrono>
#include<game.hpp.hpp>
using namespace std;
#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
int main()
{
	sf::Image image;
	sf::Texture texture;
	sf::Sprite sprite;
	
	sf::RenderWindow window(sf::VideoMode(1200, 800), "Personage and circles ");
	window.setFramerateLimit(60);//кол-во кадров за секунду

	texture.loadFromFile("img/fon.jpg");
	sf::Sprite background(texture);
	
	as::Pers p(("pers.png"),0,100, 88, 120);
	const int N = 15;
	std::vector<as::Circle*> circle;
	for (int i = 0; i <= 800; i += 800 / N)
		circle.push_back(new as::Circle(1500, i + 50, 15, rand()+50 ));

	float CurrentFrame = 0;
	sf::Clock clock;
	while (window.isOpen())
	{
		float time = clock.getElapsedTime().asMicroseconds();
		clock.restart();
		time = time / 800;
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}
		for (const auto& circle : circle)
		{
			circle->Move();
			if (circle->GetX() <= 20)
			{
				circle->SetVelocity(rand() % 5 + 1);
				circle->SetX(1500);
			}
		}
		//персонаж не выходит за левую границу
		if (p.x3 > -1)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				p.HeroDirection = 0;
				p.speed = 0.15;
				CurrentFrame += 0.005*time;
				if (CurrentFrame > 6) CurrentFrame -= 6;
				p.herosprite.setTextureRect(sf::IntRect(88 * int(CurrentFrame), 120, 88, 120));	
			}
		}
		//персонаж не выходит за правую границу
		if (p.x3 < 1115)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				p.HeroDirection = 1;
				p.speed = 0.15;
				CurrentFrame += 0.005*time;
				if (CurrentFrame > 6) CurrentFrame -= 6;
				p.herosprite.setTextureRect(sf::IntRect(88 * int(CurrentFrame), 0, 88, 120));
				
			}
		}
		//персонаж не выходит за верхнюю гарницу
		if (p.y3 > 0)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				p.HeroDirection = 2;
				p.speed = 0.2;
				CurrentFrame += 0.005*time;
				if (CurrentFrame > 2) CurrentFrame -= 2;
				p.herosprite.setTextureRect(sf::IntRect(int(CurrentFrame) * 88, 0, 88, 120));
				p.herosprite.move(0, -p.speed * time);
			}
		}
		//персонаж не выходит за нижнюю границу
		if (p.y3 < 670)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				p.HeroDirection = 3;
				p.speed = 0.2;
				CurrentFrame += 0.005*time;
				if (CurrentFrame > 4) CurrentFrame -= 2;
				p.herosprite.setTextureRect(sf::IntRect(int(CurrentFrame) * 88, 0, 88, 120));
				p.herosprite.move(0, p.speed * time);
			}
		}
		p.imagine(time);//оживляем персонажа
		window.clear();
		window.draw(background);
		window.draw(p.herosprite);
		for (const auto& circle : circle)
			window.draw(*circle->Get());
		window.display();
		std::this_thread::sleep_for(1ms);
	}
	for (const auto& circle : circle)
		delete circle;
	circle.clear();
	return 0;
	
}


